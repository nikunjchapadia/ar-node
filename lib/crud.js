/**
 * Created by nikunj on 7/12/15.
 */

var express = require("express");
var bodyParser = require("body-parser");

module.exports = function(model){

  var router = express.Router();
  router.use(bodyParser.urlencoded({extended: false}));

  function handleError(err, res){
    console.log("crud - Handling error : " + err);
    res.status(err.code || 500).send(err.message);
  }

  router.use(function(req,res,next){
    console.log("crud.js - setting content type");
    res.set("Content-Type", "text/html");
    next();
  });

  // redirect to index page
  router.get("/",function(req,res){
    res.redirect("/index");
  });

  router.get("/index", function(req,res){
    res.render("index");
  });

  // handle form post or go directly to api
  router.post("/index", function (req,res) {
    var data = req.body;
    console.log("POST body : " + JSON.stringify(data));
    model.create(data, function(err, obj){
      if(err){
        handleError(err, res);
      }
      res.redirect(req.baseUrl + '/' + obj.id);
    })
  });

  // render account after registration
  router.get('/:account', function get(req, res) {
    console.log("render new account")
    model.read(req.params.account, function (err, entity) {
      if (err) return handleError(err, res);
      res.render('/account.hbs', {
        account: entity
      });
    });
  });

  return router;
}
