var mysql = require('mysql');
var extend = require("lodash").assign;

module.exports = function(config){

  // get db connection
  function getConnection(){
    return mysql.createConnection(
      extend({database: "ar-node"}, config.mysql)
    );
  }

  // insert record
  function create(data, cb){
    console.log("create record in db");
    var connection = getConnection();
    console.log("connection : " + connection);
    var INSERT = "INSERT INTO `account` SET ?";
    connection.query(INSERT, data, function(err, res){
      if(err){
        return cb(err);
      }
      read(res.insertId, cb);
    });
    connection.end();
  }

  // get record
  function read(id, cb) {
    console.log("reading record from db");
    var connection = getConnection();
    var SELECT = "SELECT * FROM `account` WHERE `id` = ?";
    connection.query(SELECT, id, function(err, results){
      if(err){
        return cb(err);
      }
      if(!results.length) {
        return cb({code: 404, message: "Not Found"});
      }
      cb(null, results[0]);
    });
    connection.end();
  }

  // get record by username
  function getAccount(username, cb) {
    var connection = getConnection();
    var SELECT = "SELECT * FROM `account` WHERE `username` = ?";
    connection.query(SELECT, username, function(err, results){
      if(err){
        console.log("error getting user from db" + err);
        return cb(err);
      }
      if(!results.length) {
        return cb({code: 404, message: "Not Found"});
      }
      cb(null, results[0]);
    });
    connection.end();
  }

  // get list of all accounts
  function list(limit, token, cb) {
    token = token ? parseInt(token, 10) : 0;
    var connection = getConnection();
    var LIST = "SELECT * FROM `account` LIMIT ? OFFSET ?";
    connection.query(LIST, [limit, token], function(err, results) {
      if (err) {
        return cb(err);
      }
      cb(null, results, results.length === limit ? token + results.length : false);
    });
    connection.end();
  }

  return {
    createSchema : createSchema,
    list : list,
    create : create,
    read : read,
    getAccount : getAccount
  }

};

function createSchema(config){
  var connection = mysql.createConnection(
    extend({multipleStatements: true}, config)
  );

  var createAccountTable =
    "CREATE DATABASE IF NOT EXISTS `ar-node` DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_general_ci'; " +
    "USE `ar-node`; " +
      "CREATE TABLE IF NOT EXISTS `account` ( "+
      "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,"+
      "`username` varchar(64) DEFAULT NULL,"+
      "`password` varchar(255) NOT NULL,"+
      "`email` varchar(64) NOT NULL,"+
      "`zip` int(11) DEFAULT NULL,"+
      "PRIMARY KEY (`id`),"+
      "UNIQUE KEY `username` (`username`)"+
      ") ENGINE=InnoDB;";

  connection.query(createAccountTable,
    function(err, rows){
      if(err){
        throw err;
      }
      console.log("DB schema created !!!");
      connection.end(); // what happens if err does it close connection auto
    }
  )
}

if (!module.parent) {
  var prompt = require('prompt');
  prompt.start();

  prompt.get(['host', 'user', 'password'], function(err, result) {
    if (err) return;
    createSchema(result);
  });
}