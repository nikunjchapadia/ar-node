/**
 * Created by nikunj on 7/12/15.
 */

var express = require("express");
var bodyParser = require("body-parser");

module.exports = function (model) {
  var router = express.Router();
  router.use(bodyParser.json());

  router.get("/", function(req,res){
    model.list(1000, req.query.pageToken,
      function(err, entities, cursor){
        if(err){
          return handleError(err,res);
        }
        res.json({
          items : entities,
          nextPageToken : cursor
        });
      });
  });

  router.post("/", function(req,res){
    model.create(req.body,
      function(err, entity){
        if(err){
          return handleError(err,res);
        }
        res.json(entity);
      });
  });

  router.get("/validate", function (req, res) {
    console.log("handling validate");
    var username = req.query.username;
    model.getAccount(username,
      function(err, entity){
        var response = {
          valid: false,
          message: "username is not unique"
        }
        if(err){
          response.valid = true;
          response.message =  "username is unique";
        }
        res.json(response);
      });
  });

  function handleError(err, res){
    if(err.code == 404){
      return res.status(404);
    }
    res.status(500).json({
      message : err.message,
      internalCode : err.code
    });
  }

  return router;

}
