/**
 * Created by nikunj on 7/12/15.
 */
$(function () {
  console.log("I am here");

  $("#username").focusout(function (event) {
    var username = $("#username").val();
    if (username == undefined || username.length <= 4) {
      handleError("#username_msg", "username must be more than 4 chars", false);
    } else {
      $.ajax({
        type: 'GET',
        url: "/api/accounts/validate",
        data: {
          username: username,
          onlyUsername: true
        }
      }).done(function (data) {
        console.log("DATA : " + data);
        var obj = JSON.parse(data);
        handleError("#username_msg", obj.message, obj.valid);
      }).fail(function (error) {
        console.log("Error while validating username : " + error);
      });
    }
    event.preventDefault();
  });

  // handle account creation + and validation
  $("#create_account").click(function (event) {
    submitForm();
    event.preventDefault();
  });

  // validate complete form before submitting
  function submitForm() {
    resetErrorMessages();
    var username = $("#username").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var zip = $("#zip").val();
    var valid = true;
    if (password == undefined || password.length <= 6) {
      handleError("#password_msg", "password must be more than 6 chars", false);
      valid = false;
    } else {
      handleError("#password_msg", "password is valid", true);
    }
    if (zip == undefined || zip.length != 5) {
      zip = Number(zip);
      handleError("#zip_msg", "zip code must be 5 digit number", false);
      valid = false;
    } else if (!isValidNumber(zip)) {
      handleError("#zip_msg", "zip code must be 5 digit number", false);
    } else {
      handleError("#zip_msg", "zip code is valid", true);
    }
    if (email == undefined || email.length <= 5) {
      handleError("#email_msg", "email must be more than 6 chars", false);
      valid = false;
    } else if (email.indexOf("@") <= 0) {
      handleError("#email_msg", "email must contain @ char", false);
      valid = false;
    } else if (email.indexOf(".") <= 0) {
      handleError("#email_msg", "email must contain . char", false);
      valid = false;
    } else {
      handleError("#email_msg", "email is valid", true);
    }
    if (username == undefined || username.length <= 4) {
      handleError("#username_msg", "username must be more than 4 chars", false);
      valid = false;
    } else {
      $.ajax({
        type: 'GET',
        url: '/api/accounts/validate',
        data: {
          username: username
        }
      }).done(function (data, s, r) {
        console.log("DATA : " + data);
        var obj = JSON.parse(data);
        handleError("#username_msg", obj.message, obj.valid);
        if (valid && obj.valid) {
          var form = $("#registration_form");
          form.submit();
          $("#create_account").addClass("hide");
          $("#success_container").removeClass('hide').addClass('show');
        }else{
          console.log("Not submitting form");
        }
      }).fail(function (r, s, e) {
        console.log("Error validating form");
      });
    }
  }

  function isValidNumber(number){
    var valid = true;
    try {
      Number(number);
    } catch (e) {
      console.log(e);
      valid = false;
    }
    return valid;
  }

  $("#create_new_account").click(function () {
    window.location = "/index";
  });

  function resetErrorMessages() {
    $("#username_msg").text("");
    $("#password_msg").text("");
    $("#email_msg").text("");
    $("#zip_msg").text("");
  }

  function handleError(selector, message, valid) {
    if (!valid) {
      $(selector).text(message);
      $(selector).addClass("alert-danger failure_icon");
    } else {
      $(selector).text(message);
      $(selector).removeClass("alert-danger failure_icon").addClass("success_icon");
    }
  }
})