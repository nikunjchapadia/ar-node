var express = require("express");
var path = require("path");
var config = require("./config");

var app = express();

// set views and engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");
app.set("trust-proxy", true);

// setup static content
app.use(express.static('public'));
// google app engine
app.use(require('./lib/appengine-handlers'));

//account mysql model
var model = require("./lib/model-mysql")(config);
// pass model to crud operation and render html files
app.use("/", require("./lib/crud")(model));
// pass model to api crud operation and get json response back
app.use("/api/accounts", require("./lib/api")(model));

// redirect all request to account registration
app.get("/", function(req,res){
  res.redirect("/index");
});

// handle error
app.use(function(err, req,res, next){
  console.error(err.message);
  res.status(500).send("Error " + err.message);
});

var server = app.listen(process.env.PORT || config.port, "0.0.0.0", function () {
  console.log('App listening @ http://%s:%s', server.address().address, server.address().port);
});
